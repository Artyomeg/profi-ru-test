<?php

namespace App\Http\Controllers;

use App\Category;
use App\Goods;
use Illuminate\Http\Request;

class GoodsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['only' => 'store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource filtered by category.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function goodsByCategory($id)
    {
        return Goods::where('categoryId', $id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'categoryId' => 'required',
            'title'      => 'required'
        ]);

        // if no category have this id return false
        if (!Category::find($request->input('categoryId'))) {
            return false;
        }

        return Goods::create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'categoryId' => 'required',
            'title'      => 'required'
        ]);

        // if no category have this id return false
        if (!Category::find($request->input('categoryId'))) {
            return false;
        }

        return Category::find($id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Goods::findOrFail($id);

        return $task->delete();
    }
}
