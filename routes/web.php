<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// #authorize
Route::get('/home', 'HomeController@index');


// all categories routes
Route::resource('category', 'CategoryController', ['only' => [
    'index', 'store', 'update', 'destroy'
]]);

// all goods routes
Route::resource('goods', 'GoodsController', ['only' => [
    'store', 'update', 'destroy'
]]);
Route::get('/goods/by-category/{id}', 'GoodsController@goodsByCategory')->name('goods.by-category');

Route::get('/404', function () {
    return abort(404);
});